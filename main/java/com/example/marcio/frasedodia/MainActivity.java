package com.example.marcio.frasedodia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private TextView textoNovaFrase;
    private Button botaoNovaFrase;
    private String[] frases = {
            "Todo mundo quer ir pro céu, mas ninguém quer morrer.",
            "Você não conseguirá vencer a guerra contra o mundo se não conseguir vencer a guerra contra a sua própria mente.",
            "Encare o que fez de errado com motivação, pois é isso que o ajudará a fazer certo da próxima vez.",
            "A motivação não acontece por acaso, como tudo na vida você tem de batalhar para a alcançar.",
            "Há dias que você tem que ir para a frente só com o que você tem na mão, não dá para esperar pela motivação.",
            "Assim como os pássaros, precisamos aprender a superar os desafios que nos são apresentados, para alçarmos voos mais altos.",
            "Se ao enfrentar os problemas da vida lhe parecer que está escalando uma montanha impossível, lembre-se que a paisagem que avistará no topo compensará qualquer esforço seu.",
            "O poder está dentro de você, na sua mente, pois se acreditar que consegue não haverá obstáculo capaz de impedir o seu sucesso.",
            "Se quer viver uma vida feliz, amarre-se a uma meta, não a pessoas nem a coisas."};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textoNovaFrase = findViewById(R.id.textNovaFraseId);
        botaoNovaFrase = findViewById(R.id.buttonNovaFraseId);

        botaoNovaFrase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random random = new Random();
                int numAleatorio = random.nextInt(frases.length);
                textoNovaFrase.setText(frases[numAleatorio]);
            }
        });

    }
}
